export function routerConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('home', {
      //url: '',
      templateUrl: 'app/main/main.html'//,
      //controller: 'MainController',
      //controllerAs: 'vm'
    })
    .state('auth', {
      url: '/',
      templateUrl: 'app/auth/auth.html',
      controller: 'AuthController',
      controllerAs: 'vm',
      // parent : 'home',
      data: {
        'noLogin': true
      }
    })
    .state('register', {
      url: '/register',
      templateUrl: 'app/register/register.html',
      controller: 'RegisterController',
      controllerAs: 'vm',
      parent : 'home'
    })
    .state('documents', {
      url: '/documents',
      templateUrl: 'app/documents/documents.html',
      controller: 'DocumentsController',
      controllerAs: 'vm',
      parent : 'home'
    })
    .state('selling', {
      url: '/selling',
      templateUrl: 'app/selling/selling.html',
      controller: 'SellingController',
      controllerAs: 'vm',
      parent : 'documents'
    })
    .state('supply', {
      url: '/supply',
      templateUrl: 'app/supply/supply.html',
      controller: 'SupplyController',
      controllerAs: 'vm',
      parent : 'documents'
    })
    .state('payments', {
      url: '/payments',
      templateUrl: 'app/payments/payments.html',
      controller: 'PaymentsController',
      controllerAs: 'vm',
      parent : 'documents'
    })
    .state('staff', {
      url: '/staff',
      templateUrl: 'app/staff/staff.html',
      controller: 'StaffController',
      controllerAs: 'vm',
      parent : 'documents'
    })
    .state('other', {
      url: '/other',
      templateUrl: 'app/other/other.html',
      controller: 'OtherController',
      controllerAs: 'vm',
      parent : 'documents'
    })
    .state('history', {
      url: '/history',
      templateUrl: 'app/history/history.html',
      controller: 'HistoryController',
      controllerAs: 'vm',
      parent : 'documents'
    })
    .state('settings', {
      url: '/settings',
      templateUrl: 'app/settings/settings.html',
      controller: 'SettingsController',
      controllerAs: 'vm',
      parent : 'documents'
    });

  $urlRouterProvider.otherwise('/');
}
